package AvroKafka;

import com.opencsv.CSVReader;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

public class Csv2dArray {
    static String[][] readFile(String filePath) throws IOException {
        // file reader object
        FileReader fileReader = new FileReader(filePath);
        CSVReader csvReader = new CSVReader(fileReader);

        // reading csv file and writing data into 2d array
        int row = getLineNumber(filePath);
        String[] header = csvReader.readNext();
        String[] record;
        String[][] allRecords = new String[row -1][header.length];
        row = 0;
        while ((record = csvReader.readNext()) != null) {
            allRecords[row] = Arrays.copyOf(record, record.length);
            row++;
        }
        return allRecords;
    }

    static void producer(String[][] csvData) throws IOException {
        // topic name
        String topicName = "kafkaAvro";

        // schema (parsing json file)
        Schema schema = new Schema.Parser().parse(new File("Employee.avsc"));

        // set properties up
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        // creating producer object. <Key, Value>. also we need to add properties here into producer object
        Producer<String, GenericRecord> producer = new KafkaProducer<>(props);

        // getting field names from schema
        String[] fieldNames = new String[csvData[0].length];
        int num = 0;
        for(Schema.Field field : schema.getFields()){
            fieldNames[num] = field.name();
            num++;
        }

        // sending messages
        for (int i = 0; i < csvData.length; i++) {
            // creating avro schema as generic record
            GenericRecord record = new GenericData.Record(schema);

            // writing data inside record object which is created based on schema
            for (int j = 0; j < csvData[0].length; j++) {
                record.put(fieldNames[j], csvData[i][j]);
            }
            ProducerRecord<String, GenericRecord> producerRecord = new ProducerRecord<>(topicName, "Key-" + i, record);
            producer.send(producerRecord);
        }

        // closing producer
        producer.close();
        System.out.println("Producer has sent the message");
    }

    static void consumer() {
        // topic name and consumer group name
        String groupName = "Consumer-Group-Name";
        String topicName = "kafkaAvro";

        // set properties up
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName()); // here we use deserializer
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, false); // ask here
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        // creating kafka consumer object
        KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topicName));

        // creating infinite loop to listen kafka broker and receive messages continuously
        while (true) {
            ConsumerRecords<String, GenericRecord> records = consumer.poll(500);
            for (ConsumerRecord<String, GenericRecord> record : records) {
                System.out.println("Key= " + record.key() + "\tAvro Data= " + record.value());
            }
        }
    }

    private static int getLineNumber(String filePath) throws IOException {
        FileReader fileReader = new FileReader(filePath);
        CSVReader csvReader = new CSVReader(fileReader);
        int count = 0;
        while (csvReader.readNext() != null) {
            count++;
        }
        return count;
    }
}
