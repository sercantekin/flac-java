package AvroKafka;

import java.io.IOException;

public class Main {
    public static void main( String[] args ) throws IOException {
        String filePath = "./entities.csv";

        // reading CSV file, store the data to 2d array, send based on schema structure
//        String[][] csvData = Csv2dArray.readFile(filePath);
//        Csv2dArray.producer(csvData);
//        Csv2dArray.consumer();

        // sending messages while reading csv file
        CsvWhileReading.producer(filePath);
        CsvWhileReading.consumer();
    }

    /*
        questions:
        1. advantage of using linkedList
        2. Csv2dArray line number 102
    */
}
