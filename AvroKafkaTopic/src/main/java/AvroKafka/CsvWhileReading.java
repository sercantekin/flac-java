package AvroKafka;

import com.opencsv.CSVReader;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Properties;

public class CsvWhileReading {
    static void producer(String filePath) throws IOException {
        // topic name
        String topicName = "kafkaAvro";

        // schema (parsing json file)
        Schema schema = new Schema.Parser().parse(new File("Employee.avsc"));

        // set properties up
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        // creating producer object. <Key, Value>. also we need to add properties here into producer object
        Producer<String, GenericRecord> producer = new KafkaProducer<>(props);

        // creating record object
        GenericRecord record = new GenericData.Record(schema);

        // file reader object
        FileReader fileReader = new FileReader(filePath);
        CSVReader csvReader = new CSVReader(fileReader);

        // getting field names from schema, read data, send while reading
        String[] header = csvReader.readNext();
        String[] eachLine;
        String[] fieldNames = new String[header.length];
        int num = 0;
        for(Schema.Field field : schema.getFields()){
            fieldNames[num] = field.name();
            num++;
        }

        int key = 0;
        while ((eachLine = csvReader.readNext()) != null) {
            for (int i = 0; i < header.length; i++){
                record.put(fieldNames[i], eachLine[i]);
            }
            ProducerRecord<String, GenericRecord> producerRecord = new ProducerRecord<>(topicName, "This is key: " + key, record);
            producer.send(producerRecord);
            key++;
        }

        // closing producer
        producer.close();
        System.out.println("Producer has sent the message");
    }

    static void consumer() {
        // topic name and consumer group name
        String groupName = "Consumer-Group-Name";
        String topicName = "kafkaAvro";

        // set properties up
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName()); // here we use deserializer
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, false); // ask here
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        // creating kafka consumer object
        KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topicName));

        // creating infinite loop to listen kafka broker and receive messages continuously
        while (true) {
            ConsumerRecords<String, GenericRecord> records = consumer.poll(500);
            for (ConsumerRecord<String, GenericRecord> record : records) {
                System.out.println("Key= " + record.key() + "\tAvro Data= " + record.value());
            }
        }
    }
}
