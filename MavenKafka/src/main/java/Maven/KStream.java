package Maven;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class KStream {
    public void runStream () {
        Properties props = new Properties();
        // set-up properties
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-pipe");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092,localhost:9093");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        // creating streamBuilder object
        final StreamsBuilder builder = new StreamsBuilder();

        org.apache.kafka.streams.kstream.KStream<String, String> stream1 = builder.stream("streams-plaintext-input");
        // processing stream by lambda function
        org.apache.kafka.streams.kstream.KStream<String, String> stream2 = stream1.map((k, v) -> new KeyValue<>(k, String.valueOf(v.length())));
        stream2.to("streams-pipe-output");
        //builder.stream("streams-plaintext-input").to("streams-pipe-output");

        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
