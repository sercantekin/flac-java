package Maven;
/*
    There are three different approaches to send message
    1. send and forget: send a message and do not follow if message is received or not
    2. synchronous send: send message and wait for success message. after send command, we need to add get() to receive success message
    3. asynchronous send: send message with callback function to check if message sending is successful or not

    Two type of offset
    1. current offset: current position in which there is message which we need to send request to poll to read it
    2. committed offset: last position offset successfully read a message
*/
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Collections;
import java.util.Properties;

class ProConAPI {
    static void producer() {
        // message's details to send
        String topicName = "streams-plaintext";
        String key = "Key";
        String value = "Value";

        // set properties up. below 3 properties are mandatory. there are some other additional properties also available
        Properties props = new Properties();
        // bootstrap: list of kafka brokers. recommendation is to create two brokers. (second one is back-up)
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092,localhost:9093");
        // kafka sends only array of bytes, so we need to convert our key-value pair to bytes. this is what Serdes for
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName()); // there are also int and double serializers
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName()); // here we use serializer

        // creating producer object. <Key, Value>. also we need to add properties here into producer object
        Producer<String, String> producer = new KafkaProducer<>(props);

        // sending messages
        for (int i = 0; i < 10; i++) {
            // creating producer record object to send message's details
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topicName, key + i, value + i);
            // and finally we are sending the message
            producer.send(producerRecord);
        }

        // closing producer
        producer.close();

        System.out.println("Producer has sent the message");
    }

    static void consumer() {
        // topic name and consumer group name
        String groupName = "Consumer-Group-Name";
        String topicName = "streams-plaintext";

        // set properties up as above. additional property is consumer application group id
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092,localhost:9093");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);
        // it is time to deserialize the received bytes
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName()); // here we use deserializer
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // creating kafka consumer object
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topicName)); // here consumer subscribe the topic

        // creating infinite loop to listen kafka broker and receive messages continuously
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100); // if no data, return in 100 ms. poll is group coordinator
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Supplier value= " + record.value() + "\tSupplier topic name= " + record.topic());
            }
        }
    }
}
