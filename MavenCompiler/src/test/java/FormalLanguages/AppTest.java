package FormalLanguages;
/*
    - here there is only test sample to understand
    - when to create a test to see that compiler reads each char without missing any, below problems are appeared
        1. since compiler skis whitespaces, double and single quotes, input and output data are not same
        2. even if compiler does not skip any char, main function does not return anything so that it can be comparable
        in this case structure needs to be changed.
        3. main code is divided into smaller functions, and all of them boolean type functions
*/
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AppTest {
    @Test
    public void sampleTest() {
        int number1 = 10;
        int number2 = 5;
        int result = Main.sumForSample(number1, number2);
        int expected = number1 + number2;
        assertEquals(result, expected);
    }
}
