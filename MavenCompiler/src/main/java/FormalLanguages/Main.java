package FormalLanguages;
/*
    - enums are added
    - constants are split into literals
    - project pushed to git
*/
import FormalLanguages.common.Token; // enum here for some constants
import java.io.*;
import java.util.*;

public class Main {

    private static List<String> identifierArray = new ArrayList<>();
    private static List<String> delimitersArray = new ArrayList<>();
    private static List<String> keywordsArray = new ArrayList<>();
    private static List<String> stringLiteralsArray = new ArrayList<>();
    private static List<String> integerLiteralsArray = new ArrayList<>();
    private static List<String> floatLiteralsArray = new ArrayList<>();
    private static List<String> charLiteralsArray = new ArrayList<>();
    private static List<Integer> tokensArray = new ArrayList<>();

    public static void main(String[] args) {

        try {
            String fileAddress = "/home/sercan/projects/flac/_working_folders/myOwnProjectCode.txt";

            // file reading steps
            File file = new File(fileAddress);
            FileReader fr = new FileReader(file);
            LineNumberReader reader = new LineNumberReader(fr);
            StringBuilder res = new StringBuilder();
            boolean err = false;

            // driving code
            int character;
            while ((character = reader.read()) != -1) {
                char c = (char) character;
                if(err){ // case1: after err = true, here it will read the data to next delimiter
                    while ((character = reader.read()) != -1) {
                        c = (char) character;
                        if (delimiterSearch(String.valueOf(c)) || Character.isWhitespace(c)){
                            break;
                        }
                        res.append(c);
                    }
                    break;
                } else if(c == '.') { // case2: when char is dot, it will redirect the flow to related function
                    res.append(c);
                    err = x2(reader, res);
                } else if(Character.isDigit(c)) { // case3: when char is digit, it will redirect the flow to related function
                    res.append(c);
                    err = x3(reader, res);
                } else if (Character.isAlphabetic(c) || c == '_' || c == '$') { // case4: when char is either alphabetic or underscore or dollar sign
                    res.append(c);
                    err = x4(reader, res);
                } else if (c == '/') { // case5: when char is "/", it will redirect the flow to related function
                    res.append(c);
                    x5(reader, res);
                } else if (c == '"') { // case6: when char is double quotes, it will redirect the flow to related function
                    err = x6(reader, res);
                } else if (c == '\'') { // case7: when char is single quotes, it will redirect the flow to related function
                    err = x7(reader, res);
                } else if(delimiterSearch(String.valueOf(c))){ // case8: when char is delimiter, it will redirect the char to lexical mapping
                    lexicalMapping(String.valueOf(c), Token.DELIMITER);
                } else if(Character.isWhitespace(c)) { // case2: when char is whitespace, it will be ignored
                    // IGNORE
                } else {
                    res.append(c);
                    err = true;
                }
            }

            // after reading whole text file above, we need to either write the data or error
            if(err) {
                System.out.println("ERROR: " + res.toString() + " --> Error line number: " + reader.getLineNumber() + " <--");
            } else {
                System.out.println("Identifiers:");
                identifierArray.forEach(System.out::println);

                System.out.println("\nKeywords:");
                keywordsArray.forEach(System.out::println);

                System.out.println("\nString Literals:");
                stringLiteralsArray.forEach(System.out::println);

                System.out.println("\nInteger Literals:");
                integerLiteralsArray.forEach(System.out::println);

                System.out.println("\nFloat Literals:");
                floatLiteralsArray.forEach(System.out::println);

                System.out.println("\nChar Literals:");
                charLiteralsArray.forEach(System.out::println);

                System.out.println("\nDelimiters:");
                delimitersArray.forEach(System.out::println);

                System.out.println("\nAll Tokens:");
                tokensArray.forEach(System.out::println);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // functions for main code
    // when first char is dot, after first char this function will be called
    // it is to decide either constant or delimiter
    private static boolean x2(BufferedReader reader, StringBuilder res) throws IOException {
        boolean err = false;
        int character;
        char c;
        while(!err && (character = reader.read()) != -1 && character != 6) {
            c = (char) character;
            if(Character.isDigit(c)) {
                res.append(c);
                continue;
            } else if (Character.isWhitespace(c)) {
                if(res.length() == 1){
                    lexicalMapping(res.toString(), Token.DELIMITER);
                }else{
                    lexicalMapping(res.toString(), Token.FLOAT);
                }
                res.setLength(0);
                return false;
            } else if (c == '/') {
                if(res.length() == 1){
                    lexicalMapping(res.toString(), Token.DELIMITER);
                    lexicalMapping(String.valueOf(c), Token.DELIMITER);
                }else{
                    lexicalMapping(res.toString(), Token.FLOAT);
                    res.setLength(0);
                    res.append(c);
                    x5(reader, res);
                }
                res.setLength(0);
                return false;
            } else if (delimiterSearch(String.valueOf(c))) {
                if(res.length() == 1){
                    lexicalMapping(res.toString(), Token.DELIMITER);
                }else{
                    lexicalMapping(res.toString(), Token.FLOAT);
                }
                lexicalMapping(String.valueOf(c), Token.DELIMITER);
                res.setLength(0);
                return false;
            } else if (Character.isAlphabetic(c) ||  c == '_' || c == '$'){
                if(res.length() == 1){
                    lexicalMapping(res.toString(), Token.DELIMITER);
                    res.setLength(0);
                    res.append(c);
                    err = x4(reader, res);
                    break;
                }else{
                    res.append(c);
                    err = true;
                    break;
                }
            }
            res.append(c);
            err = true;
        }
        return err;
    }

    // when first char is digit, after first char this function will be called
    // it is to decide either constant or delimiter
    private static boolean x3(BufferedReader reader, StringBuilder res) throws IOException {
        boolean err = false;
        int character;
        char c;
        while(!err && (character = reader.read()) != -1 && character != 6) {
            c = (char) character;
            if (Character.isDigit(c)) {
                res.append(c);
            } else if (c == '.') {
                res.append(c);
                err = x2(reader, res);
                break;
            } else if (Character.isWhitespace(c)) {
                lexicalMapping(res.toString(), Token.INTEGER);
                res.setLength(0);
                break;
            } else if (c == '/') {
                lexicalMapping(res.toString(), Token.INTEGER);
                res.setLength(0);
                res.append(c);
                x5(reader, res);
                break;
            } else if (delimiterSearch(String.valueOf(c))){
                lexicalMapping(res.toString(), Token.INTEGER);
                res.setLength(0);
                lexicalMapping(String.valueOf(c), Token.DELIMITER);
                break;
            } else {
                res.append(c);
                err = true;
            }
        }
        return err;
    }

    // when first char alphabetic, underscore or dollar sign. after first char this function will be called
    // it is to decide either identifier or keyword or delimiter
    private static boolean x4(BufferedReader reader, StringBuilder res) throws IOException {
        int character;
        char c;
        while((character = reader.read()) != -1 && character != 6) {
            c = (char) character;
            if (Character.isDigit(c) || Character.isAlphabetic(c) || c == '_' || c == '$') {
                res.append(c);
            }else if (Character.isWhitespace(c) && !binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.IDENTIFIER);
                break;
            }else if (Character.isWhitespace(c) && binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.KEYWORD);
                break;
            }else if (c == '/' && !binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.IDENTIFIER);
                res.setLength(0);
                res.append(c);
                x5(reader, res);
                break;
            }else if (c == '/' && binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.KEYWORD);
                res.setLength(0);
                res.append(c);
                x5(reader, res);
                break;
            }else if (delimiterSearch(String.valueOf(c)) && !binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.IDENTIFIER);
                lexicalMapping(String.valueOf(c), Token.DELIMITER);
                break;
            }else if (delimiterSearch(String.valueOf(c)) && binaryKeywordSearch(res.toString())){
                lexicalMapping(res.toString(), Token.KEYWORD);
                lexicalMapping(String.valueOf(c), Token.DELIMITER);
                break;
            }else{
                res.append(c);
                return true;
            }
        }
        res.setLength(0);
        return false;
    }

    // when first char is '/'. it is to recognize the comments and ignore them. if it is not comment, then it will set the buffer reader to previous char and return
    private static void x5(BufferedReader reader, StringBuilder res) throws IOException {
        int character;
        char c;
        reader.mark(1);
        while ((character = reader.read()) != -1 && character != 6){
            c = (char) character;
            if(c == '/') {
                while ((character = reader.read()) != -1) {
                    if (character == 10) {
                        res.setLength(0);
                        return;
                    }
                }
            }else if (c == '*'){
                while ((character = reader.read()) != -1) {
                    if (character == 42) {
                        if((character = reader.read()) != -1 && character == 47){
                            res.setLength(0);
                            return;
                        }
                    }
                }
            }else{
                lexicalMapping(res.toString(), Token.DELIMITER);
                res.setLength(0);
                reader.reset();
                return;
            }
        }
    }

    // when first char is double quotes. it is to recognize the string and store accordingly
    private static boolean x6(BufferedReader reader, StringBuilder res) throws IOException {
        int character;
        char c;
        while ((character = reader.read()) != -1 && character != 6){
            c = (char) character;
            if (c == '"' && res.length() > 1 && res.charAt(res.length()-1) == '\\' && res.charAt(res.length()-2) == '\\'){
                break;
            }else if (c == '"' && res.charAt(res.length()-1) == '\\'){
                res.append(c);
            }else if (c == '"'){
                break;
            }else if (character == 3){ // end of text ascii char
                return true;
            }else{
                res.append(c);
            }
        }
        lexicalMapping(res.toString(), Token.STRING);
        res.setLength(0);
        return false;
    }

    // when first char is single quotes. it is to recognize the char and store accordingly
    private static boolean x7(BufferedReader reader, StringBuilder res) throws IOException {
        int character;
        char c;
        while ((character = reader.read()) != -1 && character != 6){
            c = (char) character;
            if (c == '\'' && res.charAt(0) == '\\' && res.length() == 1){
                res.append(c);
            }else if (c == '\'' && res.charAt(0) == '\\' && res.length() == 2){
                break;
            }else if (c == '\'' && res.length() == 1){
                break;
            }else if (character == 3){ // end of text ascii char
                return true;
            }else if (c == '\'' && res.length() > 1){
                return true;
            }else{
                res.append(c);
            }
        }
        lexicalMapping(res.toString(), Token.CHAR);
        res.setLength(0);
        return false;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // lists with information, search algorithms and mapping
    // all keywords
    private static final String[] keywordsList = {
            "abstract",   "assert",   "boolean",   "break",     "byte",      "case",
            "catch",      "char",     "class",     "const",     "continue",  "default",      "do",
            "double",     "else",     "enum",      "exports",   "extends",   "false",        "final",
            "finally",    "float",    "for",       "goto",      "if",        "implements",   "import",
            "instanceof", "int",      "interface", "long",      "module",    "native",       "new",
            "null",       "package",  "private",   "protected", "public",    "requires",     "return",
            "short",      "static",   "strictfp",  "super",     "switch",    "synchronized", "this",
            "throw",      "throws",   "transient", "true",      "try",       "void",   "volatile", "while"
    };

    // binary search algorithm for keywords
    private static boolean binaryKeywordSearch(String idName){
        int start = 0;
        int end = keywordsList.length -1;
        if (idName.length() < 2 || idName.length() > 13){
            return false;
        }
        while (start <= end){
            int middle = (start + end)/2;
            if (keywordsList[middle].equals(idName)){
                return true;
            } else if(keywordsList[middle].charAt(0) < idName.charAt(0)){
                start = middle + 1;
            } else if(keywordsList[middle].charAt(0) > idName.charAt(0)){
                end = middle - 1;
            } else {
                for (int i = 1; i < idName.length(); i++){
                    if(keywordsList[middle].charAt(i) < idName.charAt(i)){
                        start = middle + 1;
                        break;
                    } else if(keywordsList[middle].charAt(i) > idName.charAt(i)){
                        end = middle - 1;
                        break;
                    } else if (i == keywordsList[middle].length() - 1 || i == idName.length() - 1){
                        return false;
                    }
                }
            }
        }
        return false;
    }

    // all delimiters
    private static final String[] delimiterList = {
            ",", ";", "?", "!", "-", "+", ":", "@", "[", "]", "(", ")", "{", "}", "*", "<", ">", "=", "%", "&", "|", "\"", "\'", ".", "\\", "/"
    };

    // delimiter search function
    private static boolean delimiterSearch(String del){
        for (String s : delimiterList) {
            if (s.equals(del)) {
                return true;
            }
        }
        return false;
    }

    // mapping parameters and function
    private static int identifierIDs = 0;
    private static int keywordIDs = 301;
    private static int stringLiteralIDs = 601;
    private static int integerLiteralIDs = 901;
    private static int floatLiteralIDs = 1201;
    private static int charLiteralIDs = 1501;
    private static int delimiterIDs = 1801;

    private static Map<String, Integer> lexicalIdentifier = new HashMap<>();
    private static Map<String, Integer> lexicalDelimiter = new HashMap<>();
    private static Map<String, Integer> lexicalKeyword = new HashMap<>();
    private static Map<String, Integer> lexicalString = new HashMap<>();
    private static Map<String, Integer> lexicalInteger = new HashMap<>();
    private static Map<String, Integer> lexicalFloat = new HashMap<>();
    private static Map<String, Integer> lexicalChar = new HashMap<>();
    private static void lexicalMapping (String value, Token type){
        if (type.equals(Token.IDENTIFIER) && identifierIDs < Token.IdentifierLIMIT.getLimit() && !lexicalIdentifier.containsKey(value)) {
            lexicalIdentifier.put(value, identifierIDs);
            identifierArray.add(value + ":" + identifierIDs);
            identifierIDs++;
        } else if (type.equals(Token.KEYWORD) && keywordIDs < Token.KeywordLIMIT.getLimit() && !lexicalKeyword.containsKey(value)) {
            lexicalKeyword.put(value, keywordIDs);
            keywordsArray.add(value + ":" + keywordIDs);
            keywordIDs++;
        } else if (type.equals(Token.STRING) && stringLiteralIDs < Token.StringLIMIT.getLimit() && !lexicalString.containsKey(value)) {
            lexicalString.put(value, stringLiteralIDs);
            stringLiteralsArray.add(value + ":" + stringLiteralIDs);
            stringLiteralIDs++;
        } else if (type.equals(Token.INTEGER) && integerLiteralIDs < Token.IntegerLIMIT.getLimit() && !lexicalInteger.containsKey(value)) {
            lexicalInteger.put(value, integerLiteralIDs);
            integerLiteralsArray.add(value + ":" + integerLiteralIDs);
            integerLiteralIDs++;
        } else if (type.equals(Token.FLOAT) && floatLiteralIDs < Token.FloatLIMIT.getLimit() && !lexicalFloat.containsKey(value)) {
            lexicalFloat.put(value, floatLiteralIDs);
            floatLiteralsArray.add(value + ":" + floatLiteralIDs);
            floatLiteralIDs++;
        } else if (type.equals(Token.CHAR) && charLiteralIDs < Token.CharLIMIT.getLimit() && !lexicalChar.containsKey(value)) {
            lexicalChar.put(value, charLiteralIDs);
            charLiteralsArray.add(value + ":" + charLiteralIDs);
            charLiteralIDs++;
        } else if (type.equals(Token.DELIMITER) && delimiterIDs < Token.DelimiterLIMIT.getLimit() && !lexicalDelimiter.containsKey(value)) {
            lexicalDelimiter.put(value, delimiterIDs);
            delimitersArray.add(value + ":" + delimiterIDs);
            delimiterIDs++;
        } else if (Token.IdentifierLIMIT.getLimit() == identifierIDs || Token.KeywordLIMIT.getLimit() == keywordIDs
                || Token.StringLIMIT.getLimit() == stringLiteralIDs || Token.IntegerLIMIT.getLimit() == integerLiteralIDs
                || Token.FloatLIMIT.getLimit() == floatLiteralIDs || Token.CharLIMIT.getLimit() == charLiteralIDs
                || Token.DelimiterLIMIT.getLimit() == delimiterIDs) {
            System.out.println("The numbers which are reserved for " + type + " have been fully allocated.");
            System.exit(0);
        }

        switch (type){
            case IDENTIFIER:
                tokensArray.add(lexicalIdentifier.get(value));
                break;
            case KEYWORD:
                tokensArray.add(lexicalKeyword.get(value));
                break;
            case STRING:
                tokensArray.add(lexicalString.get(value));
                break;
            case INTEGER:
                tokensArray.add(lexicalInteger.get(value));
                break;
            case FLOAT:
                tokensArray.add(lexicalFloat.get(value));
                break;
            case CHAR:
                tokensArray.add(lexicalChar.get(value));
                break;
            case DELIMITER:
                tokensArray.add(lexicalDelimiter.get(value));
                break;
        }
    }

    // test purposed simple sum function
    static int sumForSample(int number1, int number2) {
        return (number1 + number2);
    }
}
