package FormalLanguages.common;

public enum Token {
    // enum elements
    IDENTIFIER("IDENTIFIER"),
    STRING("STRING"),
    INTEGER("INTEGER"),
    FLOAT("FLOAT"),
    CHAR("CHAR"),
    KEYWORD("KEYWORD"),
    DELIMITER("DELIMITER"),

    IdentifierLIMIT(301),
    KeywordLIMIT(601),
    StringLIMIT(901),
    IntegerLIMIT(1201),
    FloatLIMIT(1501),
    CharLIMIT(1801),
    DelimiterLIMIT(2101);


    // string constructor
    Token(String type) {
    }

    // integer constructor and get attribute
    private int limitReturn;
    Token(Integer limit) {
        limitReturn = limit;
    }
    public int getLimit() {
        return limitReturn;
    }

}