package DAO;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;

public interface DAOInter<T> {
    public void getEntityById() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException;
    public void getAllList() throws SQLException;
    public void insertEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException;
    public void deleteEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException;
    public void updateEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException;
}
