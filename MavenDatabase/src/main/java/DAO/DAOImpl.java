package DAO;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DAOImpl<T> implements DAOInter {
    // when DAO class object is created, a class object needs to be passed, and constructor will define class name, fields name
    private String className;
    private ArrayList<String> fields = new ArrayList<>();
    private ArrayList<String> methods = new ArrayList<>();
    private T clazz;
    private Connection c;

    public DAOImpl(T clazz, Connection c) {
        this.c = c;
        this.clazz = clazz;
        this.className = clazz.getClass().getSimpleName();
        for (Field field : clazz.getClass().getSuperclass().getDeclaredFields()) {
            fields.add(field.getName());
        }
        for (Field field : clazz.getClass().getDeclaredFields()) {
            fields.add(field.getName());
        }
        for (Method method : clazz.getClass().getSuperclass().getDeclaredMethods()) {
            methods.add(method.getName());
        }
        Collections.sort(methods);
        for (Method method : clazz.getClass().getDeclaredMethods()) {
            methods.add(method.getName());
        }
    }

    @Override
    public void getEntityById() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getID = clazz.getClass().getDeclaredMethod("getID");
        System.out.println("----------- Search in " + className + " table by ID=" + getID.invoke(clazz) + " -----------");

        // building sql command string
        StringBuilder stringBuilder = new StringBuilder("select ");
        for (String field : fields) {
            stringBuilder.append(field).append(", ");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 2); // delete last comma
        stringBuilder.append("from ").append(className).append(" where id=").append(getID.invoke(clazz));

        // prepare statement and execute
        PreparedStatement stat = c.prepareStatement(stringBuilder.toString());
        ResultSet res = stat.executeQuery();

        // printing header for output
        for (String field : fields) {
            System.out.print(field.toUpperCase() + " ");
        }
        System.out.println();

        // printing values from database
        while (res.next()){
            for (String field : fields) {
                System.out.print(res.getString(field) + " ");
            }
            System.out.println();
        }
    }

    @Override
    public void getAllList() throws SQLException {
        PreparedStatement stat = c.prepareStatement("select * from " + className);
        ResultSet res = stat.executeQuery();
        System.out.println("----------- All " + className + " Table -----------");

        // printing header for output
        for (String field : fields) {
            System.out.print(field.toUpperCase() + " ");
        }
        System.out.println();

        // printing values from database
        while (res.next()){
            for (String field : fields) {
                System.out.print(res.getString(field) + " ");
            }
            System.out.println();
        }
    }

    @Override
    public void insertEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // invoke method. create method object and pass the method name. then pass the object instance we need to invoke from
        Method getName = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(1));
        Method getSalary = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(2));
        Method getSurname = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(3));
        Method getChildMethod = clazz.getClass().getDeclaredMethod(methods.get(6));

        PreparedStatement stat = c.prepareStatement("insert into " + className + "(\"" + fields.get(3) + "\",\"" + fields.get(1)
                + "\",\"" + fields.get(2) + "\",\"" + fields.get(4) + "\") values(\'" + getSalary.invoke(clazz) + "\',\'"
                + getName.invoke(clazz) + "\',\'" + getSurname.invoke(clazz) + "\',\'" + getChildMethod.invoke(clazz) + "\')");
        stat.executeUpdate();
    }

    @Override
    public void deleteEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getID = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(0));

        PreparedStatement stat = c.prepareStatement("delete from " + className + " where id=" + getID.invoke(clazz));
        stat.executeUpdate();
    }

    @Override
    public void updateEntity() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // invoke method. create method object and pass the method name. then pass the object instance we need to invoke from
        Method getID = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(0));
        Method getName = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(1));
        Method getSalary = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(2));
        Method getSurname = clazz.getClass().getSuperclass().getDeclaredMethod(methods.get(3));
        Method getChildMethod = clazz.getClass().getDeclaredMethod(methods.get(6));

        PreparedStatement stat = c.prepareStatement("update " + className + " set \"" + fields.get(3) + "\" = \'" + getSalary.invoke(clazz)
                + "\' ,\"" + fields.get(1) + "\" = \'" + getName.invoke(clazz) + "\' ,\"" + fields.get(2) + "\" = \'" + getSurname.invoke(clazz)
                + "\' ,\"" + fields.get(4) + "\" = \'" + getChildMethod.invoke(clazz) + "\' where id=" + getID.invoke(clazz));
        stat.executeUpdate();
    }
}
