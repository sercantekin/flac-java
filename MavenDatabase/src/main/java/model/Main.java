package model;
/*
    - database name is "employee", user name is "sercan", password is "1250", hostname is "localhost"
    - Inheritance structure as follows:
     -----------------------(Mother)-----------------------
                            Employee
     --------------------(Child classes)-------------------
         Engineer          Accountant         Human Resource

    - id is generated automatically, and it cannot be duplicated across the tables. to do that only one sequence is used to generate serial type id number
    - general table (employee) has been created automatically. no data manipulation on it (there is inheritance)

    ----------------------- METHODS -----------------------
    - to insert data; create class object with structure in which there are variables, and get values from class (expect id)
    - to delete, and search data; create object with empty structure, set id, and get id by object
    - to update data; create object with structure in which there are variables, additionally set id, and get all values from object
*/
import DAO.DAOImpl;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main {
    public static void main( String[] args ) {
        Connection c = null;
        try {
            // database connection
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/employee", "sercan", "1250");
            System.out.println("Opened database successfully\n");

            // creating classes and DAO generic objects by passing class object inside
            // engineer searches
            Engineer engineer = new Engineer("generic", "update", 1000, "success");
            engineer.setID(10);
            DAOImpl<Engineer> engineerDAO = new DAOImpl<>(engineer, c);
            engineerDAO.getEntityById();
            engineerDAO.getAllList();

            // accountant
            Accountant accountant = new Accountant("name",  "lastname", 1500, "local");
            DAOImpl<Accountant> accountantDAO = new DAOImpl<>(accountant, c);
            accountant.setID(14);
            accountantDAO.getEntityById();
            accountantDAO.getAllList();

            // closing database
            c.close();
            System.out.println("\nClosed database successfully");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}
