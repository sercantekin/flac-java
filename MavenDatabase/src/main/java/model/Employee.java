package model;

public class Employee {
    private long id;
    private String name;
    private String surname;
    private int salary;

    Employee() {}
    Employee(String name, String surname, Integer salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public int getSalary() { return salary; }
    public long getID() { return id; }
    void setID(long id) { this.id = id; }
}
