package model;

public class HR extends Employee {
    private String experience;
    HR() {}
    HR(String name, String surname, Integer salary, String experience) {
        super(name, surname, salary);
        this.experience = experience;
    }
    public String getExperience() { return experience; }

    @Override
    public long getID() { return super.getID(); }
}
