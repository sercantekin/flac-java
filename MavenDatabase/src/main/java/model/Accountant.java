package model;

public class Accountant extends Employee {
    private String standard;
    Accountant() {}
    Accountant(String name, String surname, Integer salary, String standard) {
        super(name, surname, salary);
        this.standard = standard;
    }
    public String getStandard() { return standard; }

    @Override
    public long getID() { return super.getID(); }
}
