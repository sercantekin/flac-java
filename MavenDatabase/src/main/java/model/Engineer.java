package model;

public class Engineer extends Employee {
    private String language;
    Engineer() {}
    Engineer(String name, String surname, Integer salary, String language) {
        super(name, surname, salary);
        this.language = language;
    }
    public String getLanguage() { return language; }

    @Override
    public long getID() { return super.getID(); }
}
